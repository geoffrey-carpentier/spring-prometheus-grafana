FROM adoptopenjdk:11-jre-hotspot as builder
WORKDIR application
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} application.jar
RUN java -Djarmode=layertools -jar application.jar extract

FROM adoptopenjdk:11-jre-hotspot
WORKDIR application
COPY --from=builder application/dependencies/ ./
COPY --from=builder application/spring-boot-loader/ ./
COPY --from=builder application/snapshot-dependencies/ ./
COPY --from=builder application/application/ ./
ENTRYPOINT ["java" , "org.springframework.boot.loader.JarLauncher"]

# Ce Dockerfile contient un builder qui sert de base à la création de l'image finale en effectuant des opérations intermédiaires.
# Ici le builder permet d'optimiser le processus de création de l'image, en créant une première image contenant les dépendances.
# Cette image va servir de couche (layer), dans la seconde image. Les prochaines création d'image Docker seront alors plus rapide (tant que les dépendances du projet ne changent pas).
# L'utilisation d'un builder, n'est pas obligatoire et dépend des technologies embarquées dans le container (langage, etc.).