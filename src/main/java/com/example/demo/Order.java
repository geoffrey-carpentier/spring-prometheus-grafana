package com.example.demo;

public class Order {
    private final long id;
    private final String productName;
    private final String firstName;
    private final String lastName;
    private final String address;

    public Order(long id, String productName, String firstName, String lastName, String address) {
        this.id = id;
        this.productName = productName;
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
    }

    public long getId() {
        return id;
    }

    public String getProductName() {
        return productName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getAddress() {
        return address;
    }
    
}
