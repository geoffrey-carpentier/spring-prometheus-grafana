
package com.example.demo;


import java.util.HashMap;
import java.util.Map;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;

@RestController
public class OrderController {

	private Counter ordersCounter;
	private Map<String, Counter> ordersCounterByProduct = new HashMap<>();
	private Map<String, Counter> ordersCounterByAddress = new HashMap<>();
	private MeterRegistry registry;

	public OrderController(MeterRegistry registry) {
		this.registry = registry;
		ordersCounter = registry.counter("order.counter");
	}

	@PostMapping("/order")
	Order newOrder(@RequestBody Order newOrder){
		String productName = newOrder.getProductName();
		String address = newOrder.getAddress();

		ordersCounterByProduct.putIfAbsent(productName, registry.counter("order.productName", "label", productName));
		ordersCounterByAddress.putIfAbsent(address, registry.counter("order.address", "label", address));

		ordersCounterByProduct.get(productName).increment();
		ordersCounterByAddress.get(address).increment();
		ordersCounter.increment();
		return newOrder;
	}
}