Feature: Order-post

    Try to create an order with a post request

    Scenario: Testing valid POST endpoint

    Given url "http://localhost:8080/order"
    And request { "id": 1, "lastName": "Dupont", "firstName": "Pierre", "address": "Lille", "productName": "Casquette" }
    When method POST
    Then status 200
    And match response != null